# -*- coding: utf-8 -*-

from io import StringIO
from tokenize import String
from odoo import models, fields, api

#bagian daftar aset
class daftar_aset(models.Model):
      _name = 'tabel_aset'
      
      name = fields.Char(string="ID Aset")
      nama_aset = fields.Char(string="Nama Aset")      
      merek = fields.Char(string="Merek Aset")
      jumlah = fields.Integer()
      harga_per_unit = fields.Integer()
      harga_total = fields.Float(compute="_value_pc", store=True)      
      keterangan = fields.Text()
      foto  = fields.Binary() 

      @api.depends('harga_per_unit', 'jumlah' )
      def _value_pc(self):
          self.harga_total = float(self.harga_per_unit) * self.jumlah

      @api.multi
      def cetak_pengadaan(self):
        return self.env['models'].get_action(self, 'kmm_manajemen_aset.laporan_pengadaan_document')



#bagian member
class member(models.Model):
      _name = 'tabel_member'
            
      name = fields.Char(string="Id Anggota")      
      nama_lengkap = fields.Char(string="Nama Lengkap")      
      jabatan = fields.Char(string="jabatan", default="tayooooo")
      alamat_ktp = fields.Text()
      domisili_sekarang = fields.Text()
      email = fields.Char()
      no_hp = fields.Char(string="no hp")
      status = fields.Char(string="status")
      ruang_kerja = fields.Char(string="ruang kerja")
      tanggal_aktif  = fields.Date()
      tanggal_non_aktif = fields.Date()
      jenis_kelamin = fields.Selection([
        ('laki-laki','Laki-Laki'),
        ('perempuan','Perempuan'),
      ], string='Jeni Kel')
      foto  = fields.Binary()
      agama = fields.Char(string="agama")
      divisi = fields.Char(string="Divisi")
      
      @api.multi
      def cetak_pengadaan(self):
        return self.env['models'].get_action(self, 'kmm_manajemen_aset.laporan_pengadaan_document')

   

#bagian pengadaan aset
class pengadaan_aset(models.Model):
      _name = 'tabel_pengadaan_aset'

      name = fields.Char(string="Nama Aset")
      # id_aset = fields.Many2one('manajemen_aset.daftar_aset', ondelete='cascade', string="nama Aset", required=True)      
      merek = fields.Char(string="Merek Aset")
      jumlah = fields.Integer()
      harga_per_unit = fields.Integer()
      harga_total = fields.Integer()      
      tanggal_pengadaan = fields.Date()
      kode_pengadaan = fields.Char(string="kode pengadaan")
      status_pengadaan = fields.Char(string="status pengadaan")   
      nama_pengaju = fields.Char(string="nama pengaju")
      id_pengaju = fields.Char(string="id pengaju")
      petugas_aproval = fields.Char(string="petugas aproval")
      id_petugas = fields.Char(string="id petugas")
      keterangan = fields.Text()


      @api.multi
      def cetak_pengadaan(self):
        return self.env['models'].get_action(self, 'kmm_manajemen_aset.laporan_pengadaan_document')

#       @api.depends('value')
#       def _value_pc(self):
#           self.value2 = float(self.value) / 100



#bagian peminjaman aset
class peminjaman_aset(models.Model):
      _name = 'tabel_peminjaman_aset'

      name = fields.Char(string="Nama Aset")
      # id_aset = fields.Many2many('manajemen_aset.daftar_aset', ondelete='cascade', string="nama Aset", required=True)
      
      merek = fields.Char(string="Merek Aset")
      jumlah_pinjam = fields.Integer()
      tanggal_peminjaman = fields.Date()
      kode_peminjaman = fields.Char(string="kode peminjaman")
      keperluan  = fields.Text()
      lokasi_penggunaan = fields.Char(string="lokasi penggunaan")
      tanggal_kembali = fields.Date()   
      status_peminjaman = fields.Char(string="nama pengaju")
      nama_peminjam = fields.Char(string="nama peminjam")
      id_peminjam = fields.Char(string="id peminjam")
      petugas_aproval = fields.Char(string="petugas aproval")
      id_petugas = fields.Char(string="id petugas")
      keterangan = fields.Text()

#       @api.depends('value')
#       def _value_pc(self):
#           self.value2 = float(self.value) / 100




#bagian inventaris aset
class inventarisasi_aset(models.Model):
      _name = 'tabel_inventaris_aset'

      name = fields.Char(string="Nama Aset")
      id_inventaris  = fields.Char(string="id inventaris")
      merek = fields.Char(string="Merek Aset")
      jumlah_aset = fields.Integer()
      kondisi_baik = fields.Integer()
      kondisi_rusak = fields.Integer()      
      kondisi_dalam_perbaikan = fields.Integer()
      tanggal_inventaris = fields.Date()
      tanggal_dimusnahkan = fields.Date()   
      lokasi_aset = fields.Char(string="lokasi aset")
      harga_per_unit = fields.Char(string="harga per unit")
      harga_total_aset = fields.Char(string="harga total aset")
      tanggal_pengadaan = fields.Date()
      kode_pengadaan = fields.Char(string="kode pengadaan")
      status_pengadaan = fields.Char(string="status pengadaan")
      nama_pengaju = fields.Char(string="nama pengaju")
      id_pengaju = fields.Char(string="id pengaju")
      petugas_aproval = fields.Char(string="petugas aproval")
      id_petugas = fields.Char(string="id petugas")
      keterangan = fields.Char(string="keterangan")
      kode_inventaris = fields.Char(string="kode inventaris")
      petugas_inventaris = fields.Char(string="petugas inventaris")

      @api.multi
      def cetak_pengadaan(self):
        return self.env['models'].get_action(self, 'kmm_manajemen_aset.laporan_pengadaan_document')


#       @api.depends('value')
#       def _value_pc(self):
#           self.value2 = float(self.value) / 100



#bagian laporan aset
# class laporan(models.Model):
#       _name = 'tabel_laporan_aset'

#       name = fields.Char(string="judul laporan")
#       id_laporan = fields.Char(string="id laporan")
#       tanggal_terbit = fields.Date()
#       keterangan_tambahan = fields.Char(string="keterangan tambahan")
      
# #       @api.depends('value')
# #       def _value_pc(self):
# #           self.value2 = float(self.value) / 100